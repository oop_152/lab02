public class JavaVariables5 {
    public static void main(String[] args) {
        final int myNum = 15;
        // myNum = 20; //will gennerate an error: cannot assign a value to a final variadle
        System.out.println(myNum);
    }
}
