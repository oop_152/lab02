import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in) ;
        String strScore ;
        int score ;
        String grade ;
        System.out.print("Please in put your score : ");
        strScore = sc.next() ;
        score = Integer.parseInt(strScore) ;

        if (score >= 80) {
            grade = "A" ;
        } else if (score >= 75) {
            grade = "B+" ;
        } else if (score >= 70) {
            grade = "B" ;
        } else if (score >= 65) {
            grade = "C+" ;
        } else if (score >= 60) {
            grade = "C" ;
        } else if (score >= 55) {
            grade = "D+" ;
        } else if (score >= 50) {
            grade = "D" ;
        } else {
            grade = "F" ;
        }
        System.out.println("Grede : " + grade);
    }
}
